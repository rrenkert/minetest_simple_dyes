-- -------------------------------------------------------------------
-- Copyright (C) 2013 by Sascha Wilde <wilde@sha-bang.de>
--
-- This program is free software under the GNU LGPL (>=v2.1)
-- Read the file README.txt coming with the software for details.
-- -------------------------------------------------------------------

-- This mod provides recipes to craft some basic dyes from the
-- standard dye mod provided by the "common" standard game base.


-- Crafting Items

minetest.register_craftitem("simple_dyes:fermented_grass", {
	description = "Fermented Grass",
	inventory_image = "simple_dyes_fermented_grass.png",
})

minetest.register_craft({
        type = "shapeless",
        output = "simple_dyes:fermented_grass",
        recipe = {'default:grass_1', 'default:gravel', 'bucket:bucket_water'},
	replacements = {{"bucket:bucket_water", "bucket:bucket_empty"}}
})


-- Dye Recipes

local dye_pigments = {
        {'black', 'default:coal_lump'},
        {'red', 'default:clay_brick'},
        {'green', 'default:cactus'},
        {'blue', 'simple_dyes:fermented_grass'},
        {'yellow', 'default:sand'},
        {'white', 'default:glass'},
}


for _, row in ipairs(dye_pigments) do
	local name = 'dye:'..row[1]
	local ingredient = row[2]
        minetest.register_craft({
                type = "shapeless",
                output = name..' 6',
                recipe = {ingredient, 'default:clay_lump', 'bucket:bucket_water'},
        	replacements = {{"bucket:bucket_water", "bucket:bucket_empty"}}
        })
end
