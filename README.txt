Minetest mod: simple_dyes
=========================

This mod adds recipes to Minetest which allow for crafting some of the
dyes defined by the standard dye mod, which is part of the standard
"common" base game.

Only few recipes are provided, many more dyes can be crafted by mixing
the basic colors, which is a feature of the standard dye mod.

You can always find the latest version of this mod at:
https://hg.intevation.org/fun/minetest-simple_dyes


Installation
------------

Rename the top-level directory of the mod to "simple_dyes" if
necessary and place it MINETEST-BASE-DIRECTORY/mods/minetest/.
For further information and OS specific hints see:
http://wiki.minetest.com/wiki/Installing_Mods


Recipes
-------

All dyes are crafted from three ingredients:

  Water Bucket  +  Clay Lump  +  PIGMENT

where the PIGMENT determines the color of the dye:

  DYE:     PIGMENT:
  black    Coal Lump
  red      Clay Brick
  green    Cactus
  blue     Fermented Grass
  yellow   Sand
  white    Glass

"Fermented Grass" is a item defined by this mod, as there are no
suitable natural blue pigments in the standard game.  It is crafted
from grass like this:

  Grass  +  Water Bucket  +  Gravel


License of source code
----------------------

Copyright (C) 2013 Sascha Wilde <wilde@sha-bang.de>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation; either version 2.1 of the License, or
(at your option) any later version.

http://www.gnu.org/licenses/lgpl-2.1.html


License of media (textures and sounds)
--------------------------------------
Copyright (C) 2013 Sascha Wilde <wilde@sha-bang.de>

Attribution-ShareAlike 3.0 Unported (CC BY-SA 3.0)                                 
http://creativecommons.org/licenses/by-sa/3.0/
